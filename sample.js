var a = 10;
var b = 20;

var name = 'hello world xcv    name'

var arr = [10, 20, 30, 54];


var obj1 = {
	val1: 15,
	val2: [13, 15, 17],
	val3: function(val1, val2) {
			return val1 + val2;
	}
}



var sum = function(val1, val2) {
	return val1 + val2;
}

var sub

var result = sum(arr[1], arr[2]);

// console.log('length: ', name[0]);
// console.log(typeof obj1);
// console.log('second value is: ', obj1.val2);
// console.log('result is: ', obj1.val3(obj1.val1, b));

// console.log(Object.keys(obj1));
// console.log(Object.values(obj1));

arr.forEach((val) => {
	// console.log('value: ', val)
})

for(var i=0; i < arr.length; i++) {
	// console.log(arr[i]);
}


// Value and Reference types:

// value type
var a = 10;

var b = a;

a = 20;

// console.log('a: ', a);
// console.log('b: ', b);


// Reference type
var obj1 = {
	id: 1, val: 10
};

// object deep copy

var obj2 = {id: obj1.id, val: obj1.val}; // -> deep copy  not recommending

var obj3 = Object.assign({}, obj1);  // shallow copy

obj1.val = 20;

obj2.val = 30;

obj3.val = 50;

console.log('obj1.val: ', obj1.val);
console.log('obj2.val: ', obj2.val);
console.log('obj3.val: ', obj3.val);


// let,  -->  block scope

// const  -->  immutable

if(true) {
	var val1 = 16;
}

if(true) {
	const val2 = 24;
	// console.log('val2: ', val2);
}

// console.log('val1: ', val1);

// console.log('val2: ', val2);

// object destrucring

let newObj = {
	...obj1  //spread operator
}

obj1.val = 60;

console.log('newObj.val: ', newObj.val);


let fname = 'test';
let dept = 20;

// let emp = {
// 	fname: fname,
// 	dept: dept
// };

let newEmp = {
	...obj1,
	fname,
	dept,
	isRep: true
}

let {isRep, id, val} = {...newEmp};

// console.log('emp:', emp);
console.log('newEmp', newEmp);
console.log('isRep', isRep);
console.log('id', id);
console.log('val', val);

let arr1 = [20, 30, 40];

let arr2 = [...arr1];

console.log('arr1: ', arr1);

console.log('arr2: ', arr2);


// arrow functions


const add = function (val1, val2) {
	return val1 + val2;
}

const addition = (val1, val2) => val1 + val2;

const inc10 = val => val + 10;

// let addition = (val1, val2) =>  { 
// 	val1 = val1 + 10;
// 	return val1 + val2; 
// }

// () => returnStatement

// () => {

// 	return returnStatement
// }

console.log(add(23, 45));

console.log(addition(27, 35));

console.log(inc10(16));